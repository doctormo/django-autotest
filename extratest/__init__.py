"""Django extra useful test base classes"""
default_app_config = 'extratest.app.AutotestConfig'
__pkgname__ = 'django-extratest'
__version__ = '1.6'
