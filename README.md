.. image:: https://raw.githubusercontent.com/doctormo/django-extratest/master/icon.png
 :class: floating-box
 :alt: Django AutoTest

Description
===========

Provides extra base test functionality for django test suites.

Installation
============

Install via pip or your python egg layer of choice:

pip install django-extratest

No other apps are required except for django core.

Base Test Classes
=================

extratest.base contains useful base classes which can help you write tests for django.
The functionality includes getObj for getting objects of a certain type. assertGet/Post
for making requests with status checking and forum error checking. As well as client
and user management functions.

Development
===========

Please come help this project by using the code, writing tests and leting the
project authors know you appreciate their code:

http://gitlab.com/doctormo/django-extratest
